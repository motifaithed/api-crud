const express = require('express');
const tasksController = require('../controllers/tasks.controller');

const router = express.Router();

router.post('/create', tasksController.createTask);
router.get('/', tasksController.getTasks);
router.put('/:id/update',tasksController.updateTask);
router.delete('/:id/delete',tasksController.deleteTask);

module.exports = router;